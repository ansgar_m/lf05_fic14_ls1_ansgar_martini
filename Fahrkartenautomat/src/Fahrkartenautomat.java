﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static double fahrkartenbestellungErfassen(Scanner tastatur) 
    {
    	double zuZahlenderBetrag = 0;
    	int Ticketnummer = 0;
    	double Ticketpreiß = 0;
//    	System.out.printf("Wälen Sie ihre Wunschfahrkarte für Berlin AB aus: \n");
//    	System.out.printf("Einzelfahrschein Regeltarif AB [2,90 EUR] (1) \n");
//    	System.out.printf("Tageskarte Regeltarif AB [8,60 EUR] (2) \n");
//   	System.out.printf("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3) \n");
//    	System.out.printf("Bezahlen (9) \n\n");
//    	System.out.print("Ihre Wahl: ");
//    	Ticketnummer = tastatur.nextInt();
//    	while(Ticketnummer >= 4 || Ticketnummer < 1 && Ticketnummer != 9) {
//    		System.out.println(">>falsche Eingabe<<");
//    		System.out.println("Ihre Wahl: ");
//    		Ticketnummer = tastatur.nextInt();
//    	}
    	while(Ticketnummer != 9) {
        	System.out.printf("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n");
        	System.out.printf("Einzelfahrschein Regeltarif AB [2,90 EUR] (1) \n");
        	System.out.printf("Tageskarte Regeltarif AB [8,60 EUR] (2) \n");
        	System.out.printf("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3) \n");
        	System.out.printf("Bezahlen (9) \n\n");
        	System.out.print("Ihre Wahl: ");
        	Ticketnummer = tastatur.nextInt();
        	
        	while((Ticketnummer >= 4 || Ticketnummer < 1) && Ticketnummer != 9) {
        		System.out.println(">>falsche Eingabe<<");
        		System.out.println("Ihre Wahl: \n\n");
        		Ticketnummer = tastatur.nextInt();
        	}
        			if(Ticketnummer == 1) {
        				Ticketpreiß = 2.90;
        				System.out.printf("Anzahl der Tickets: ");
        				int TICKETANZAHL = tastatur.nextInt();
        				while(TICKETANZAHL < 1 || TICKETANZAHL > 10) {
        					System.out.println(">>Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n\n");
        					System.out.printf("Anzahl der Tickets: ");
        					TICKETANZAHL = tastatur.nextInt();
        				}
        				zuZahlenderBetrag += TICKETANZAHL * Ticketpreiß;
        				System.out.printf("Zwischensumme: %.2f \n\n", zuZahlenderBetrag);
        			} 
        			else if(Ticketnummer == 2){
        				Ticketpreiß = 8.60;
        				System.out.printf("Anzahl der Tickets: ");
        				int TICKETANZAHL = tastatur.nextInt();
        				while(TICKETANZAHL < 1 || TICKETANZAHL > 10) {
        					System.out.println(">>Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n\n");
        					System.out.printf("Anzahl der Tickets: ");
        					TICKETANZAHL = tastatur.nextInt();
        				}
        				zuZahlenderBetrag += TICKETANZAHL * Ticketpreiß;
        				System.out.printf("Zwischensumme: %.2f \n\n", zuZahlenderBetrag);
        			}
        			else if(Ticketnummer == 3) {
        				Ticketpreiß = 23.50;
        				System.out.printf("Anzahl der Tickets: ");
        				int TICKETANZAHL = tastatur.nextInt();
        				while(TICKETANZAHL < 1 || TICKETANZAHL > 10) {
        					System.out.println(">>Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n\n");
        					System.out.printf("Anzahl der Tickets: ");
        					TICKETANZAHL = tastatur.nextInt();
        				}
        				zuZahlenderBetrag += TICKETANZAHL * Ticketpreiß;
        				System.out.printf("Zwischensumme: %.2f \n\n", zuZahlenderBetrag);
        			}
//    		else if(Ticketnummer == 9){
//    		
//    		}
    	}
//    	System.out.printf("Anzahl der Tickets: ");
//    	int TICKETANZAHL = tastatur.nextInt();
//    	while(TICKETANZAHL < 1 || TICKETANZAHL > 10) {
//    		System.out.println(">>Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n\n");
//    		System.out.printf("Anzahl der Tickets: ");
//    		TICKETANZAHL = tastatur.nextInt();
//    	}
//    	zuZahlenderBetrag += TICKETANZAHL * Ticketpreiß;
    	
        	return zuZahlenderBetrag;
    }
    public static double fahrkartenbezahlen(Scanner tastatur, double eingeworfeneMünze, double zuZahlenderBetrag, double TICKETANZAHL) 
    {
    	double rückgabebetrag;
    	//int ticket_anzahl;
    	double eingezahlterGesamtbetrag;
    	eingezahlterGesamtbetrag = 0.0;
    	//ticket_anzahl = (int) (zuZahlenderBetrag/TICKETPREIß);8
        
    		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    		{
    			//System.out.println("Ticketpreiß (Euro): " + TICKETPREIß);
    			//System.out.printf("Anzahl der Tickets: %d \n", ticket_anzahl);
    			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    			eingeworfeneMünze = tastatur.nextDouble();
    			eingezahlterGesamtbetrag += eingeworfeneMünze;
    		}

    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	return rückgabebetrag;
    }
    static void warte(int millisekunde) {
        try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public static void fahrkartenAusgeben() 
    {
    	System.out.println("\nFahrschein wird ausgegeben");
    	
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
          // try {
 			//Thread.sleep(250);
 		//} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			//e.printStackTrace();
 		//}
        }
        System.out.println("\n\n");
    }
    
    static void muenzeAusgeben(double rückgabebetrag, String einheit) {
    	 while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
         {
      	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
         }
         while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
         {
      	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
         }
         while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
         {
      	  System.out.println("50 CENT");
      	  	  rückgabebetrag -= 0.5;
         }
         while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
         {
      	  System.out.println("20 CENT");
      	  	  rückgabebetrag -= 0.2;
         }
         while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
         {
      	  System.out.println("10 CENT");
      	  	  rückgabebetrag -= 0.1;
         }
         while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
         {
      	  System.out.println("5 CENT");
      	  	  rückgabebetrag -= 0.05;
         }
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
     	   
     	   muenzeAusgeben(rückgabebetrag, "EURO");
     	 
     	   System.out.printf("\n\n Vergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen! \n");
     	   System.out.println("Wir wünschen Ihnen eine gute Fahrt.");
//            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
//            {
//         	  System.out.println("2 EURO");
// 	          rückgabebetrag -= 2.0;
//            }
//            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
//            {
//         	  System.out.println("1 EURO");
// 	          rückgabebetrag -= 1.0;
//            }
//            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
//            {
//         	  System.out.println("50 CENT");
// 	          rückgabebetrag -= 0.5;
//            }
//            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
//            {
//         	  System.out.println("20 CENT");
//  	          rückgabebetrag -= 0.2;
//            }
//            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
//            {
//        	  System.out.println("10 CENT");
// 	          rückgabebetrag -= 0.1;
//            }
//            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
//            {
//         	  System.out.println("5 CENT");
//  	          rückgabebetrag -= 0.05;
//            }
        }
    }
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur); 
       //double eingezahlterGesamtbetrag;
       double eingeworfeneMünze = 0.0;
       double Ticketpreiß = 2.5;
       double rückgabebetrag = fahrkartenbezahlen(tastatur, eingeworfeneMünze, zuZahlenderBetrag, Ticketpreiß);
       //int ticket_anzahl;

       //System.out.print("Zu zahlender Betrag (EURO): ");
       //zuZahlenderBetrag = tastatur.nextDouble();

       // Geldeinwurf
       // -----------
       //eingezahlterGesamtbetrag = 0.0;
       //ticket_anzahl = (int) (zuZahlenderBetrag/TICKETPREIß);
        
       		//fahrkartenbestellungErfassen(tastatur);
       //while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       //{
    	 //  System.out.println("Ticketpreiß (Euro): " + TICKETPREIß);
       //  System.out.printf("Anzahl der Tickets: %d \n", ticket_anzahl);
       //  System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
       //  System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
       //  eingeworfeneMünze = tastatur.nextDouble();
       //  eingezahlterGesamtbetrag += eingeworfeneMünze;
       //  
       //}
       
       
       //fahrkartenbezahlen(tastatur, eingeworfeneMünze, zuZahlenderBetrag, TICKETPREIß);
       
       
       // Fahrscheinausgabe
       // -----------------
       //System.out.println("\nFahrschein wird ausgegeben");
       //for (int i = 0; i < 8; i++)
       //{
       // System.out.print("=");
       // try {
       //	Thread.sleep(250);
       //} catch (InterruptedException e) {
			// TODO Auto-generated catch block
       //	e.printStackTrace();
       //}
       fahrkartenAusgeben();
       rueckgeldAusgeben(rückgabebetrag);
       
       }
    //System.out.println("\n\n");
     
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
    // rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    // if(rückgabebetrag > 0.0)
    // {
    //   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    //   System.out.println("wird in folgenden Münzen ausgezahlt:");
    //
    //     while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
    //     {
    //  	  System.out.println("2 EURO");
    //        rückgabebetrag -= 2.0;
    //     }
    //     while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
    //     {
    //  	  System.out.println("1 EURO");
    //        rückgabebetrag -= 1.0;
    //     }
    //     while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
    //     {
    //  	  System.out.println("50 CENT");
    //        rückgabebetrag -= 0.5;
    //     }
    //     while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
    //     {
    //  	  System.out.println("20 CENT");
    //        rückgabebetrag -= 0.2;
    //     }
    //     while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
    //     {
    //   	  System.out.println("10 CENT");
    //        rückgabebetrag -= 0.1;
    //     }
    //     while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
    //     {
    //  	  System.out.println("5 CENT");
    //        rückgabebetrag -= 0.05;
    //     }
    // }
     
    // System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    //                    "vor Fahrtantritt entwerten zu lassen!\n"+
    //                    "Wir wünschen Ihnen eine gute Fahrt.");
    

}