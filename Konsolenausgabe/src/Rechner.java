import java.util.Scanner;

public class Rechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		
		int zahl1 = myScanner.nextInt();
		
		System.out.println("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		int zahl2 = myScanner.nextInt();
		
		int ergebnis = zahl1 + zahl2;
		
		System.out.println("\n\n\nErgebnis der Addition lautet: ");
		System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
		
		myScanner.close();
	}

}
